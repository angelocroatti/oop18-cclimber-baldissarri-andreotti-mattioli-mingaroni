## Crazy Climber

Il gruppo si pone come obiettivo quello di realizzare una versione riadattata del famoso gioco arcade Crazy Climber.

Crazy Climber è un gioco arcade anni ottanta in cui il giocatore controlla uno stuntman che deve raggiungere la sommità di quattro grattacieli arrampicandosi su di essi a mani nude. Durante il suo percorso incontrerà svariati ostacoli come gli inquilini che apriranno e chiuderanno le finestre o che lancieranno vasi di fiori. Al contrario del gioco originale la nostra versione si baserà su un unico grattacielo infinitamente alto possibilmente con difficoltà incrementale, lo scopo del gioco quindi è realizzare il punteggio più alto possibile che verra salvato in classifica.


Funzionalità minimali ritenute obbligatorie:

* L’utente dovrà essere in grado di controllare lo stuntman, farlo muovere e gestire le collisioni.

* Generazione in maniera randomica del grattacielo ed anche degli ostacoli durante la scalata.

* L’utente dovrà essere in grado di salvare il proprio punteggio e di poter visualizzare la classifica anche alla riapertura dell' applicativo.

* Aggiunta di bonus che renderebbero l esperienza di gioco più dinamica.


Funzionalità opzionali:

* Aggiunta di nuovi ostacoli.


* Rendere la difficoltà del gioco incremetale.

* Aggiunta delle animazioni.


"Challenge" principali:

* Sarà necessario prevedere una facile estendibilità per quanto riguarda gli ostacoli e i bonus che si possono trovare durante il percorso.

* L' elevato numero di finestre presenti sul palazzo necessiterà particolare attenzione per non rovinare l esperienza di gioco.

* Sarà necessaria una gestione efficiente del multithreading per rendere il gioco più fluido possibile.


Suddivisione del lavoro:

* Andreotti Matteo: Gestione del protagonista e delle collisioni.

* Mattioli Alessandro: Definizione degli ostacoli e come vengono generati.

* Baldassarri Nicolas: Creazione della mappa e dell' interfaccia grafica.

* Mingaroni Anna: Implementazione e definizione dei bonus, gestione dei punteggi. 